/*
 * Default linker script for MD407 (STM32F407)
 * All code and data goes to RAM
 */

/* Memory Spaces Definitions */
MEMORY
{
  RAM (xrw)  :  ORIGIN = 0x20000000, LENGTH = 112K
}


/* 180328: Added KEEP() to .start_section to prevent
 *         --gc-sections from cleaning this section. 
 */

SECTIONS
{
    .text :
    {
        . = ALIGN(4);
        KEEP(*(.start_section)) /* startup code */
        *(.text)                /* remaining code */
        *(.text.*)
        *(.data)                /* initialised data */
        *(.data.*)
        *(.rodata)              /* read-only data (constants) */
        *(.rodata.*)
        *(.bss)                 /* uninitialised data */
        *(COMMON)
        . = ALIGN(4);
    }  >RAM
}

