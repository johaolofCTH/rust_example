pub unsafe fn write_d_moder(setting : u32) {
	let moder_register = 0x40020C00 as *mut u32;
	*moder_register = setting;
}


pub unsafe fn write_d_odr(data : u32) {
	let odr_register = 0x40020C14 as *mut u32;
	*odr_register = data;
}

pub unsafe fn write_e_moder(setting : u32) {
	let moder_register = 0x40021000 as *mut u32;
	*moder_register = setting;
}


pub unsafe fn write_e_odr(data : u32) {
	let odr_register = 0x40021014 as *mut u32;
	*odr_register = data;
}
