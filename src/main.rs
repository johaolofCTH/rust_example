#![feature(asm)]
#![feature(panic_implementation)]
#![feature(start)]
#![no_std]
#![feature(naked_functions)]
#![allow(dead_code)]

#[naked]
#[start]
#[link_section = ".start_section"]
fn startup(_argc:isize, _argv: *const *const u8) -> isize {
	setup_stack();
	main();
}

mod gpio;

use core::panic::PanicInfo;

fn setup_stack() {
	unsafe {
		asm!(  "LDR R0,=0x2001C000\n MOV SP,R0\n");
	}
}

fn main() -> ! {
	unsafe {
		gpio::write_e_moder(0x55555555);
		loop {
			gpio::write_e_odr(0xAA);
		}
	}
}

#[panic_implementation]
fn panic(_info: &PanicInfo) -> ! {

    loop {
    }
}
